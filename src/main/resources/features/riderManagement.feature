@ResetWeb @RiderManager
  Feature: In oder to Open Rider Management page successfully

    Scenario Outline:  Open Rider management page successfully
      Given I open the rider management page <website>
      Then I verify the rider management screen
      When I click rider management button
      When I click rider list button
      Then I verify Rider management page



      Examples:
        | website                                                                         |
        | https://admin-test.witzmobility.com/home                                        |

