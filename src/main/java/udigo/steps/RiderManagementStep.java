package udigo.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class RiderManagementStep extends BaseSteps {
    @Given("^I open the rider management page (.+)$")
    public void i_open_the_rider_management_page(String website)  {
        getRiderManagement().openWebsite(website);
    }

    @When("^I click rider management button$")
    public void i_click_rider_management_button() {
        getRiderManagement().clickBtnRiderManagement();
    }

    @When("^I click rider list button$")
    public void i_click_rider_list_button()  {
        getRiderManagement().clickBtnRiderList();
    }

   /* @When("^I enter check box seach Rider (.+)$")
    public void i_enter_check_box_seach_rider(String seach) {
        getRiderManagement().enterSeachChk(seach);
    }
*/
    @Then("^I verify the rider management screen$")
    public void i_verify_the_rider_management_screen()  {

    }

    @Then("^I verify Rider management page$")
    public void i_check_rider_management_page()  {
        getRiderManagement().verifyRiderScreen();
    }




}
