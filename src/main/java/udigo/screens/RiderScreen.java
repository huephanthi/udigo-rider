package udigo.screens;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import udigo.core.Driver;

public class RiderScreen extends RiderElement {
    public static WebDriver driver;
    private static RiderScreen INSTANCE;
    private static WebDriverWait wait;

    private RiderScreen() {
        initRiderInformationPage();
    }

    public static RiderScreen getInstance() {
        if (INSTANCE == null || driver == null) {
            INSTANCE = new RiderScreen();
        }
        return INSTANCE;
    }

    private void initRiderInformationPage() {
        Driver.openDriver();
        driver = Driver.getDriver();
        PageFactory.initElements(driver, this);
        wait = Driver.getWait();

    }

    public void openWebsite(String website) {
        driver.get(website);
    }

    public void closeWebsite() {
        driver.quit();
        Driver.driver = null;
    }

    public void clickBtnRiderManagement() {
        wait.until(ExpectedConditions.visibilityOf(btnRiderMngmt));
        btnRiderMngmt.click();
    }
    public void clickBtnRiderList() {
        wait.until(ExpectedConditions.visibilityOf(btnRiderList));
        btnRiderList.click();
    }
    public void verifyRiderScreen() {
        wait.until(ExpectedConditions.visibilityOf(chkSeach));
        Assert.assertNotNull(chkSeach.isDisplayed());
    }
    public void enterSeachChk(String seach){
        wait.until(ExpectedConditions.visibilityOf(chkSeach));
        chkSeach.sendKeys(seach);

    }


}
