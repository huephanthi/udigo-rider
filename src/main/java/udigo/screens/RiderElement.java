package udigo.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RiderElement {
   @FindBy(xpath = "//li[3]/a/span[1]/span")
    protected WebElement btnRiderMngmt;

   @FindBy(xpath = "//li[3]/div/ul/li/a/span/span")
    protected  WebElement btnRiderList;

   @FindBy(xpath = "//div[1]/div/span/input")
    protected  WebElement chkSeach;
}
